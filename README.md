# faulTTY Bar

This project is just a collection of markdown and latex documents for a home bar. It's primary purpose is to provide easy access and distribution of cocktail and spirit recipes for the bartender, friends and family.

---

## specs/specs.md

Simple specs for the bartender to use.

---

## menu/menu.tex

Elaborate tex template to generate a PDF presentable to friends and family.
