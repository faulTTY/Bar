# Specs for the bartender

## The Bees Knees

### Ingredients

- 50 ml Gin
- 20 ml Runny Honey
- 20 ml Fresh Lemon Juice
- Lemnon twist

Prepare lemon twist first. Mix and shake, double strain to glass **without ice**. Squeeze lemon twist, rub around, curl and garnish.

---

## London Calling

### Ingredients

- 40 ml Gin
- 15 ml Fresh Lemon Juice
- 15 ml Sherry
- 15 ml Sugar Syrup
- 2 dahes bitter

Double strain and serve in chilled coupe **without ice**.

---

## Trader Vic's Original Mai Tai

### Ingredients

- 60 ml Dark Rum
- 25 ml fresh lime juice
- 15 ml Triple Sec or Cointreau
- 10 ml Amaretto
- Mint sprig for garnish
- Lime husk for garnish

Shake and dump. Give Mint sprig gentle smack, pop in lime husk.

---

## East Indian Negroni

### Ingredients

- 45 ml Diplomatico Reserva Rum
- 20 ml Cream Sherry
- 20 ml Campari
- Orange twist to garnish

Prepare orange twist first. Pour into mixing glass, fill with ice and stir. **Serve on the rocks**. Squeeze orange twist and garnish.

---

## Artist's Special

- 30 ml Blended Scotch
- 30 ml Sherry
- 15 ml fresh lemon juice
- 15 ml berry syrup (Cassis maybe)
- Lemon twist to garnish

Prepare lemon twist first. Mix and shake, double strain to stilt glass **without ice**. Sharp twist, rub around, curl and garnish.

---

## Paper Plane

- 20 ml Elijah Craig Bourbon Whisky
- 20 ml Campari
- 20 ml Amaro Nonino (or Amaro Montenegro)
- 20 ml Lemon Juice

Prepare a **paper plane**, double strain into glass **without ice**.

---

## Sherry Cobbler

- 75 ml Sherry
- 1 dash Mr Bitters Apricot and Hickory bitters (optional)
- 2 wedges Orange
- 2 wedges Lemon
- Mint sprig to garnish
- Fresh berries to garnish

Prepare berries for garnish first. Squeeze your wedges of fruit in to your tin and then add them too. Add the sherries and bitters. Shake and dump into frozen bulky brandy glass. Garnish with mint and berries, serve with a straw.

---

## Eastside

- 60 ml Gin
- 30 ml Fresh Lime Juice
- 15 ml Sugar Syrup
- 5 Mint leaves
- Cucumber strip
- Cucumber roll to garnish

Roll one cucumber strip up and skewer it, put the other into the shaker. Double strain into chilled coupe **without ice**.

---

## Tommy's Margarita

- 60 ml Tequila Reposado
- 30 ml Fresh Lime Juice
- 15 ml Agave Syrup

Rim half the glass with salt, shake and strain **over ice** into a double old fashioned glass. Garnish with spent lime.

---

## Espresso Martini

- 45 ml Vodka
- 30 ml Espresso
- 30 ml Kahlúa
- 10 ml Vanilla Syrup
- Lemon Coin
- 3 Coffee beans to garnish

**Shake sharp**, double strain to iced martini glass. 'Con la mosca' - Health, wealth and happiness.

---

## Eggnog

- 30 ml Spiced Rum
- 30 ml Ruby Port
- 20 ml Sugar Syrup
- 20 ml Cream
- 1 Egg
- Orange Twist

**Shake dry** to mix the ingredients, fill with ice and shake again. Garnish with orange twist.

---

## Clover Club

- 40 ml Gin
- 20 ml Lime Juice
- 15 ml Creme de Cassis
- 1 Egg White

**Shake dry** to mix the ingredients, fill with ice and shake again. Double strain to Coupe.

---

## Coffee House

- 40 ml Rye Whisky
- 20 ml Coffee Liqueur
- 2 dash Orange Bitters
- 2 dash Angostura Bitters
- Orange twist to garnish

Prepare orange twist first. Pour into mixing glass, fill with ice and stir. **Serve on the rocks**. Squeeze orange twist and garnish.

---

## "Beati Fumo" di Nonino

- 50 ml Amaro Nonino
- 20 ml Islay Scotch, Laphroaig 10
- 20 ml Lemon Juice
- 20 ml Honey Syrup

Shake; strain; up.

---

## Corn \& Voodoo

- 40 ml Corn Whiskey
- 20 ml Lemon Juice
- 15 ml Maple Syrup
- 6 Bazil Leaves
- Ginger Beer

Put all ingredients except the Ginger Beer into tin, shake and double strain into Longdrink Glass **with ice**.

---

## The White Negroni

- 40 ml Gin
- 30 ml White Vermouth
- 20 ml Amaro
- Grapefruit twist to garnish

**Stir with ice** and strain into a tumbler.

---

## The Black Manhattan

- 60 ml Rye or Bourbon
- 20 ml Amaro
- 1 dash Angostura bitters
- 1 dash orange bitters

**Stir with ice** and strain into a small glass.

---

## The Rum Monte Sour

- 45 ml Amaro Montenegro
- 15 ml dark rum
- 30 ml Lemon juice
- 5 ml sugar syrup
- 15 ml egg white
- orange twist to garnish

**Shake dry** first and then **shake again with ice**, double strain into a tumbler.

---

## Strawberry Daiquiri

- 60 ml rum
- 30 ml sugar syrup
- 30 ml lime juice
- 4-5 frozen strawberries
- 1 cup crushed or small cubed ice
- Strawberry fan to garnish

Put everything into the blender and blend for a couple seconds on the highest settings. The blender will get warm, don't let it run too long. Put the result in a **cold glass** and serve.

---

## Frozen Margarita

- 45 ml blanco tequila
- 20 ml orange liquer
- 15 ml sugar syrup
- 20 ml lime juice
- 1 cup crushed or small cubed ice
- Lime wheel to garnish

Put everything into the blender and blend for a couple seconds on the highest settings. The blender will get warm, don't let it run too long. Put the result in a **cold glass** and serve.

---

## The Expedition

- 30 ml Lime Juice
- 15 ml Cinnamon Syrup
- 15 ml Honey Syrup
- 10 ml Vanilla Syrup
- 60 ml Seltzer
- 10 ml New Orleans Coffee Liquer
- 60 ml Black Blended Rum
- 30 ml Bourbon

Add all the ingredients to a drink mixer tin with crushed ice. Flash blend and open pour with gated finish into a mug or glass. Add Mint garnish.

---

## New York Sour

- 60 ml Bourbon
- 30 ml Lemon Juice
- 20 ml Sugar Syrup
- 1 dash Orange bitters
- 1 Egg white
- 10 ml Red wine float

Combine all ingredients except the wine in a cocktail shaker, shake vigorously and strain into a tumbler with a big ice cube/ball. Float the wine on top.

---

## Kahluaccino

- 30 ml Espresso
- 30 ml Whole Milk
- 10 ml Cinnamon Syrup
- 15 ml Kahlua Especial
- 30 ml Light Rum

Combine all ingredients in a cocktail shaker, shake vigorously and dump into a highball glass

---

## Cable Car

- 45ml spiced rum
- 20ml orange curacao
- 30ml lemon juice
- 10ml sugar syrup
- Garnish: cinnamon sugar rim and orange twist (Do NOT skip on the rim)

Combine all ingredients in a cocktail shaker, shake vigorously and strain into a coupe.
